<?php

/**
 * Created by PhpStorm.
 * User: johann
 * Date: 11.03.2017
 * Time: 23:03
 */
class LoginController extends Controller
{
    private $authenticationService;
    private $redirectService;

    public function __construct($serviceProvider, $view)
    {
        parent::__construct($serviceProvider, $view);

        $this->authenticationService = $serviceProvider->getAuthenticationService();
        $this->redirectService = $serviceProvider->getRedirectService();

        if ($this->authenticationService->getAuthenticated())
            $this->redirectService->redirectTo('home/index');
    }

    public function index() {

        $this->view->render('login');
    }

    public function login() {

        $success = $this->authenticationService->login();
        if ($success)
            $this->redirectService->redirectTo('home/index');
        else
            $this->redirectService->redirectTo('login/index');
    }

    public function logout() {

        $this->authenticationService->logout();
        $this->redirectService->redirectTo('login/index');
    }
}
