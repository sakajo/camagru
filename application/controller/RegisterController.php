<?php
/**
 * Created by PhpStorm.
 * User: johann
 * Date: 12.03.2017
 * Time: 01:26
 */

class RegisterController extends Controller
{
    private $registerService;
    private $redirectService;
    private $authenticationService;

    public function __construct($serviceProvider, $view) {

        parent::__construct($serviceProvider, $view);

        $this->registerService = $serviceProvider->getRegisterService();
        $this->redirectService = $serviceProvider->getRedirectService();
        $this->authenticationService = $serviceProvider->getAuthenticationService();

        if ($this->authenticationService->getAuthenticated())
            $this->redirectService->redirectTo('home/index');
    }

    public function index() {
        $this->view->render('register');
    }

    public function register() {

        $success = $this->registerService->register();
        if ($success)
            $this->redirectService->redirectTo('login/index');
        else
            $this->redirectService->redirectTo('register/index');
    }
}