<?php

/**
 * Created by PhpStorm.
 * User: johann
 * Date: 11.03.2017
 * Time: 20:00
 */
class HomeController extends Controller
{
    private $authenticationService;
    private $redirectService;

    public function __construct($serviceProvider, $view) {
        parent::__construct($serviceProvider, $view);

        $this->authenticationService = $serviceProvider->getAuthenticationService();
        $this->redirectService = $serviceProvider->getRedirectService();

        if (!$this->authenticationService->getAuthenticated())
            $this->redirectService->redirectTo('login/index');
    }

    public function index() {
        $this->view->render('home');
    }
}