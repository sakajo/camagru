<?php
/**
 * Created by PhpStorm.
 * User: johann
 * Date: 12.03.2017
 * Time: 01:28
 */
?>
<div class="container">

    <?php $this->renderFeedback(); ?>

    <div class="login-box" style="width: 50%; display: block">
        <h2>Register a new account</h2>
        <form action="<?=$this->url?>register/register" method="post">
            <input type="text" name="email" placeholder="Email address" /><br />
            <input type="text" name="first_name" placeholder="First name" /><br />
            <input type="text" name="last_name" placeholder="Last name" /><br />
            <input type="password" name="password" placeholder="Password" autocomplete="off" /><br />
            <input type="password" name="confirm-password" placeholder="Confirm password" autocomplete="off" /><br />
            <input type="submit" value="Register" />
        </form>
    </div>

</div>
