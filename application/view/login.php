<?php
/**
 * Created by PhpStorm.
 * User: johann
 * Date: 11.03.2017
 * Time: 23:08
 */
?>

<div class="container">

    <?php $this->renderFeedback(); ?>

    <div class="login-page-box">
        <div class="table-wrapper">
            <div class="login-box">
                <h2>Login here</h2>
                <form action="<?=$this->url?>login/login" method="post">
                    <input type="text" name="email" placeholder="Email" required /><br />
                    <input type="password" name="password" placeholder="Password" required /><br />
                    <input type="submit" class="login-submit-button" value="Log in" />
                </form>
            </div>
            <div>
                <h2>No account yet?</h2>
                <a href="<?=$this->url?>register/index">Register</a>
            </div>
        </div>
    </div>
</div>
