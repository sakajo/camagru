<?php

/**
 * Created by PhpStorm.
 * User: johann
 * Date: 13.03.2017
 * Time: 18:29
 */
class UserModel
{
    private $id;
    private $email;
    private $firstName;
    private $lastName;
    private $password;

    public function __construct($id, $email, $firstName, $lastName, $password) {

        $this->id = $id;
        $this->email = $email;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->password = $password;
    }

    public function getId() {
        return $this->id;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getFirstName() {
        return $this->firstName;
    }

    public function getLastName() {
        return $this->lastName;
    }

    public function getPassword() {
        return $this->password;
    }
}