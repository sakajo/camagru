<?php

/**
 * Created by PhpStorm.
 * User: johann
 * Date: 13.03.2017
 * Time: 23:39
 */

class DatabaseManager
{
    private $connection;

    public function getConnection() {

        if (!$this->databaseExists())
            $this->createDatabase();

        $this->connection = new PDO('mysql:host=' . Config::DATABASE_SERVER . ';dbname='
            . Config::DATABASE_NAME, Config::DATABASE_USERNAME, Config::DATABASE_PASSWORD);
        $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        return $this->connection;
    }

    public function closeConnection() {

        if ($this->connection != null)
            $this->connection = null;
    }

    private function databaseExists() {

        $this->connection = new PDO('mysql:host=' . Config::DATABASE_SERVER,
            Config::DATABASE_USERNAME, Config::DATABASE_PASSWORD);
        $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $result = $this->connection->query("SHOW DATABASES LIKE '" . Config::DATABASE_NAME . "'")->rowCount() == 1;
        $this->closeConnection();

        return $result;
    }

    private function createDatabase() {

        $this->connection = new PDO('mysql:host=' . Config::DATABASE_SERVER,
            Config::DATABASE_USERNAME, Config::DATABASE_PASSWORD);
        $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $query = 'CREATE DATABASE ' . Config::DATABASE_NAME . ';'
            . 'USE ' . Config::DATABASE_NAME . ';'
            . 'CREATE TABLE user('
            . 'user_id INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT, '
            . 'first_name VARCHAR(64) NOT NULL, '
            . 'last_name VARCHAR(64) NOT NULL, '
            . 'email VARCHAR(64) NOT NULL, '
            . 'password VARCHAR(64) NOT NULL);';
        $this->connection->exec($query);

        $this->closeConnection();
    }
}