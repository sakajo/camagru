<?php
/**
 * Created by PhpStorm.
 * User: johann
 * Date: 11.03.2017
 * Time: 21:51
 */
class ControllerFactory {

    private $serviceProvider;
    private $view;

    public function __construct($serviceProvider, $view) {
        $this->serviceProvider = $serviceProvider;
        $this->view = $view;
    }

    public function create($type) {

        $className = ucwords($type) . 'Controller';
        require_once Config::PATH_CONTROLLER . $className . '.php';

        switch ($type) {
            case 'error':
                return new ErrorController($this->serviceProvider, $this->view);
                break;
            case 'home':
                return new HomeController($this->serviceProvider, $this->view);
                break;
            case 'login':
                return new LoginController($this->serviceProvider, $this->view);
                break;
            case 'register':
                return new RegisterController($this->serviceProvider, $this->view);
                break;
        }
    }
}