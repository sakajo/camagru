<?php
/**
 * Created by PhpStorm.
 * User: johann
 * Date: 09.03.2017
 * Time: 22:32
 */

class ServiceProvider
{
    private $repositoryProvider;

    private $requestService;
    private $sessionService;
    private $filterService;
    private $redirectService;
    private $authenticationService;
    private $registerService;

    public function __construct($repositoryProvider) {

        $this->repositoryProvider = $repositoryProvider;

        require_once Config::PATH_SERVICE . 'FilterService.php';
        require_once Config::PATH_SERVICE . 'SessionService.php';
        require_once Config::PATH_SERVICE . 'RequestService.php';
        require_once Config::PATH_SERVICE . 'AuthenticationService.php';
        require_once Config::PATH_SERVICE . 'RedirectService.php';
        require_once Config::PATH_SERVICE . 'RegisterService.php';

        $this->requestService = new RequestService();
        $this->filterService = new FilterService();
        $this->sessionService = new SessionService($this->filterService);
        $this->redirectService = new RedirectService();
        $this->authenticationService = new AuthenticationService($this->requestService,
            $this->sessionService, $this->filterService,
            $this->repositoryProvider->getUserRepository());
        $this->registerService = new RegisterService($this->requestService,
            $this->sessionService, $this->filterService,
            $this->repositoryProvider->getUserRepository());
    }

    public function getSessionService() {
        return $this->sessionService;
    }

    public function getRequestService() {
        return $this->requestService;
    }

    public function getFilterService() {
        return $this->filterService;
    }

    public function getAuthenticationService() {
        return $this->authenticationService;
    }

    public function getRedirectService() {
        return $this->redirectService;
    }

    public function getRegisterService() {
        return $this->registerService;
    }

    public function getDatabaseService() {
        return $this->databaseService;
    }

    public function getUserService() {
        return $this->userService;
    }
}