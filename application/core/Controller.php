<?php
/**
 * Created by PhpStorm.
 * User: johann
 * Date: 09.03.2017
 * Time: 22:01
 */

class Controller
{
    protected $view;
    protected $serviceProvider;

    public function __construct($serviceProvider, $view) {
        $this->serviceProvider = $serviceProvider;
        $this->view = $view;

        $this->serviceProvider->getSessionService()->start();
    }
}