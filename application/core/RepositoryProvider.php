<?php

/**
 * Created by PhpStorm.
 * User: johann
 * Date: 14.03.2017
 * Time: 00:22
 */
class RepositoryProvider
{
    private $databaseManager;

    private $userRepository;

    public function __construct($databaseManager)
    {
        $this->databaseManager = $databaseManager;

        require_once Config::PATH_REPOSITORY . 'UserRepository.php';

        $this->userRepository = new UserRepository($this->databaseManager);
    }

    public function getUserRepository() {
        return $this->userRepository;
    }
}