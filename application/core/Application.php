<?php
/**
 * Created by PhpStorm.
 * User: johann
 * Date: 09.03.2017
 * Time: 21:58
 */

class Application
{
    private $controller;
    private $controllerType = 'home';
    private $method = 'index';
    private $parameters = array();

    private $view;
    private $controllerFactory;
    private $databaseManager;
    private $repositoryProvider;
    private $serviceProvider;

    private $requestService;
    private $sessionService;

    public function __construct() {

        $this->createDependencies();

        $this->splitUrl();
        if ($this->controllerIsAvailable()) {
            $this->loadController();
            $this->callMethod();
        } else {
            $this->loadErrorPage();
        }
    }

    private function createDependencies() {

        require_once '../application/config/Config.php';
        require_once '../application/config/Text.php';

        require_once 'Controller.php';
        require_once 'ControllerFactory.php';
        require_once 'View.php';
        require_once 'DatabaseManager.php';
        require_once 'RepositoryProvider.php';
        require_once 'ServiceProvider.php';

        $this->databaseManager = new DatabaseManager();
        $this->repositoryProvider = new RepositoryProvider($this->databaseManager);
        $this->serviceProvider = new ServiceProvider($this->repositoryProvider);

        $this->requestService = $this->serviceProvider->getRequestService();
        $this->sessionService = $this->serviceProvider->getSessionService();

        $this->view = new View($this->sessionService);
        $this->controllerFactory = new ControllerFactory($this->serviceProvider, $this->view);
    }

    private function splitUrl() {

        if ($this->requestService->findInGet('url')) {

            $url = trim($this->requestService->findInGet('url'), '/');
            $url = filter_var($url, FILTER_SANITIZE_URL);
            $url = explode('/', $url);

            $this->controllerType = isset($url[0]) ? $url[0] : Config::DEFAULT_CONTROLLER;
            $this->method = isset($url[1]) ? $url[1] : Config::DEFAULT_METHOD;

            unset($url[0], $url[1]);

            $this->parameters = array_values($url);
        }
    }

    private function controllerIsAvailable() {
        return file_exists(Config::PATH_CONTROLLER
                . ucwords($this->controllerType) . 'Controller.php');
    }

    private function loadController() {
        $this->controller = $this->controllerFactory->create($this->controllerType);
    }

    private function callMethod() {
        if (method_exists($this->controller, $this->method)) {

            if (!empty($this->parameters)) {
                call_user_func_array(array($this->controller, $this->method), $this->parameters);
            } else {
                $this->controller->{$this->method}();
            }
        } else {
            $this->loadErrorPage();
        }
    }

    private function loadErrorPage() {
        $this->controllerType = 'error';
        $this->loadController();
        $this->controller->index();
    }
}
