<?php

/**
 * Created by PhpStorm.
 * User: johann
 * Date: 11.03.2017
 * Time: 20:13
 */
class View
{
    private $sessionService;

    private $url;
    private $feedback;

    public function __construct($sessionService) {
        $this->sessionService = $sessionService;
        $this->url = Config::URL;
    }

    public function render($filename) {
        require Config::PATH_VIEW . 'templates/header.php';
        require Config::PATH_VIEW . $filename . '.php';
        require Config::PATH_VIEW . 'templates/footer.php';
    }

    public function renderFeedback() {
        $this->feedback = $this->sessionService->get('feedback');
        require Config::PATH_VIEW . 'templates/feedback.php';
        $this->sessionService->remove('feedback');
    }
}