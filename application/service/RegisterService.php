<?php

/**
 * Created by PhpStorm.
 * User: johann
 * Date: 12.03.2017
 * Time: 12:04
 */
class RegisterService
{
    private $requestService;
    private $sessionService;
    private $filterService;

    private $userRepository;

    private $email;
    private $firstName;
    private $lastName;
    private $password;
    private $confirmPassword;

    public function __construct($requestService, $sessionService,
                                $filterService, $userRepository) {

        $this->requestService = $requestService;
        $this->sessionService = $sessionService;
        $this->filterService = $filterService;

        $this->userRepository = $userRepository;
    }

    public function register() {

        $this->readInput();
        if ($this->validateInput()) {
            $this->addUserToDatabase();
            return true;
        } else {
            return false;
        }
    }

    private function readInput() {

        $this->email = $this->filterService->filter(
            $this->requestService->findInPost('email'));
        $this->firstName = $this->filterService->filter(
            $this->requestService->findInPost('first_name'));
        $this->lastName = $this->filterService->filter(
            $this->requestService->findInPost('last_name'));
        $this->password = $this->filterService->filter(
            $this->requestService->findInPost('password'));
        $this->confirmPassword = $this->filterService->filter(
            $this->requestService->findInPost('confirm-password'));
    }

    private function validateInput() {

        return $this->validateEmail()
            && $this->validateFirstName()
            && $this->validateLastName()
            && $this->validatePassword()
            && $this->checkEmailUniqueness();
    }

    private function addUserToDatabase() {

        $this->userRepository->insert($this->email,
            $this->firstName, $this->lastName, $this->password);
    }

    private function checkEmailUniqueness() {

        $user = $this->userRepository->getByEmail($this->email);
        if ($user) {
            $this->sessionService->add('feedback', Text::FEEDBACK_EMAIL_ALREADY_EXISTS);
            return false;
        } else {
            return true;
        }
    }

    private function validateEmail() {

        if (empty($this->email)) {
            $this->sessionService->add('feedback', Text::FEEDBACK_EMAIL_FIELD_EMPTY);
            return false;
        }
        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            $this->sessionService->add('feedback', Text::FEEDBACK_EMAIL_DOES_NOT_FIT_PATTERN);
            return false;
        }
        return true;
    }

    private function validateFirstName() {

        if (empty($this->firstName)) {
            $this->sessionService->add('feedback', Text::FEEDBACK_FIRST_NAME_FIELD_EMPTY);
            return false;
        }
        if (!preg_match("/^[a-zA-Z0-9]*$/", $this->firstName)) {
            $this->sessionService->add('feedback', Text::FEEDBACK_FIRST_NAME_DOES_NOT_FIT_PATTERN);
            return false;
        }
        return true;
    }

    private function validateLastName() {

        if (empty($this->lastName)) {
            $this->sessionService->add('feedback', Text::FEEDBACK_LAST_NAME_FIELD_EMPTY);
            return false;
        }
        if (!preg_match("/^[a-zA-Z0-9]*$/", $this->lastName)) {
            $this->sessionService->add('feedback', Text::FEEDBACK_LAST_NAME_DOES_NOT_FIT_PATTERN);
            return false;
        }
        return true;
    }

    private function validatePassword() {

        if (empty($this->password) || empty($this->confirmPassword)) {
            $this->sessionService->add('feedback', Text::FEEDBACK_PASSWORD_FIELD_EMPTY);
            return false;
        }
        if ($this->password !== $this->confirmPassword) {
            $this->sessionService->add('feedback', Text::FEEDBACK_PASSWORDS_DO_NOT_MATCH);
            return false;
        }
        if (strlen($this->password) < 5) {
            $this->sessionService->add('feedback', Text::FEEDBACK_PASSWORD_TOO_SHORT);
            return false;
        }
        return true;
    }
}
