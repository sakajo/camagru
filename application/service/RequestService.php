<?php
/**
 * Created by PhpStorm.
 * User: johann
 * Date: 11.03.2017
 * Time: 09:14
 */

class RequestService {

    public function findInGet($key) {
        if (isset($_GET[$key])) {
            return $_GET[$key];
        }
    }

    public function findInPost($key) {
        if (isset($_POST[$key])) {
            return $_POST[$key];
        }
    }
}