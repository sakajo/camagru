<?php

/**
 * Created by PhpStorm.
 * User: johann
 * Date: 11.03.2017
 * Time: 22:45
 */

class RedirectService
{
    public function redirectTo($path) {
        header("location: " . Config::URL . $path);
    }
}