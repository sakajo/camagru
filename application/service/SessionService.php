<?php

/**
 * Created by PhpStorm.
 * User: johann
 * Date: 11.03.2017
 * Time: 20:58
 */
class SessionService
{
    private $filterService;

    public function __construct($filterService) {
        $this->filterService = $filterService;
    }

    public function start() {
        if (session_id() == '')
            session_start();
    }

    public function stop() {
        session_destroy();
    }

    public function set($key, $value) {
        $_SESSION[$key] = $value;
    }

    public function add($key, $value) {
        $_SESSION[$key][] = $value;
    }

    public function get($key) {
        if (isset($_SESSION[$key])) {
            $value = $_SESSION[$key];
            return $this->filterService->filter($value);
        }
    }

    public function remove($key) {
        if (isset($_SESSION[$key])) {
            unset($_SESSION[$key]);
        }
    }
}