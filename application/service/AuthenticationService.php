<?php
/**
 * Created by PhpStorm.
 * User: johann
 * Date: 11.03.2017
 * Time: 21:35
 */

class AuthenticationService
{
    private $requestService;
    private $sessionService;
    private $filterService;
    private $userRepository;

    private $email;
    private $password;

    public function __construct($requestService, $sessionService, $filterService, $userRepository) {

        $this->requestService = $requestService;
        $this->sessionService = $sessionService;
        $this->filterService = $filterService;
        $this->userRepository = $userRepository;
    }

    public function login() {

        $this->readInput();
        if ($this->validateInput() && $this->tryToLogin()) {
            $this->sessionService->set('logged_in', true);
            return true;
        } else {
            return false;
        }
    }

    public function logout() {

        $this->sessionService->stop();
    }

    public function getAuthenticated() {

        return $this->sessionService->get('logged_in') ? true : false;
    }

    public function getUser() {

        if ($this->getAuthenticated())
            return $this->userRepository->getById($this->sessionService->get('user_id'));
        else
            return null;
    }

    private function readInput() {

        $this->email = $this->filterService->filter(
            $this->requestService->findInPost('email'));
        $this->password = $this->filterService->filter(
            $this->requestService->findInPost('password'));
    }

    private function validateInput() {

        if (empty($this->email)) {
            $this->sessionService->add('feedback', Text::FEEDBACK_EMAIL_FIELD_EMPTY);
            return false;
        }
        if (empty($this->password)) {
            $this->sessionService->add('feedback', Text::FEEDBACK_PASSWORD_FIELD_EMPTY);
            return false;
        }
        return true;
    }

    private function tryToLogin() {

        $user = $this->userRepository->getByEmail($this->email);
        if ($user && $user->getPassword() === $this->password) {
            $this->sessionService->set('user_id', $user->getId());
            $this->sessionService->set('logged_in', true);
            return true;
        } else {
            $this->sessionService->add('feedback', Text::FEEDBACK_WRONG_CREDENTIALS);
            return false;
        }
    }
}
