<?php

/**
 * Created by PhpStorm.
 * User: johann
 * Date: 13.03.2017
 * Time: 23:48
 */

class Config
{
    const URL = 'http://localhost/camagru/public/';

    const DATABASE_USERNAME = 'root';
    const DATABASE_PASSWORD = '';
    const DATABASE_SERVER = 'localhost';
    const DATABASE_NAME = 'Camagru';

    const PATH_CONTROLLER = '/opt/lampp/htdocs/camagru/application/controller/';
    const PATH_VIEW = '/opt/lampp/htdocs/camagru/application/view/';
    const PATH_SERVICE = '/opt/lampp/htdocs/camagru/application/service/';
    const PATH_MODEL = '/opt/lampp/htdocs/camagru/application/model/';
    const PATH_REPOSITORY = '/opt/lampp/htdocs/camagru/application/repository/';

    const DEFAULT_CONTROLLER = 'home';
    const DEFAULT_METHOD = 'index';
}