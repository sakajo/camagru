<?php

/**
 * Created by PhpStorm.
 * User: johann
 * Date: 13.03.2017
 * Time: 23:54
 */
class Text
{
    const FEEDBACK_EMAIL_FIELD_EMPTY = 'Email field was empty.';
    const FEEDBACK_EMAIL_DOES_NOT_FIT_PATTERN = 'Your email does not fit into the email pattern.';
    const FEEDBACK_FIRST_NAME_FIELD_EMPTY = "First name field was empty.";
    const FEEDBACK_FIRST_NAME_DOES_NOT_FIT_PATTERN = 'First name does not fit the name pattern. Only letters and digits are allowed, 2-64 characters.';
    const FEEDBACK_LAST_NAME_FIELD_EMPTY = "Last name field was empty.";
    const FEEDBACK_LAST_NAME_DOES_NOT_FIT_PATTERN = 'Last name does not fit the name pattern. Only letters and digits are allowed, 2-64 characters.';
    const FEEDBACK_PASSWORD_FIELD_EMPTY = 'Password field was empty.';
    const FEEDBACK_PASSWORDS_DO_NOT_MATCH = 'Passwords don\'t match.';
    const FEEDBACK_PASSWORD_TOO_SHORT = 'Password is shorter than 5 characters.';
    const FEEDBACK_USERNAME_ALREADY_EXISTS = 'Username already exists';
    const FEEDBACK_EMAIL_ALREADY_EXISTS = 'Email already exists';
    const FEEDBACK_WRONG_CREDENTIALS = 'Wrong username or password';
}