<?php

/**
 * Created by PhpStorm.
 * User: johann
 * Date: 14.03.2017
 * Time: 00:20
 */
class UserRepository
{
    private $databaseManager;

    public function __construct($databaseManager) {

        $this->databaseManager = $databaseManager;

        require_once Config::PATH_MODEL . 'UserModel.php';
    }

    public function getByEmail($email) {

        $connection = $this->databaseManager->getConnection();

        $query = 'SELECT u.user_id, u.first_name, u.last_name, u.email, u.password '
            . 'FROM user u '
            . 'WHERE u.email = \'' . $email . '\';';
        $result = $connection->query($query);

        $user = null;
        if ($result->rowCount() == 1) {
            $row = $result->fetch();
            $user = new UserModel($row['user_id'], $row['first_name'],
                $row['last_name'], $row['email'], $row['password']);
        }
        $this->databaseManager->closeConnection();

        return $user;
    }

    public function getById($id) {

        $connection = $this->databaseManager->getConnection();

        $query = 'SELECT u.user_id, u.first_name, u.last_name, u.email, u.password '
            . 'FROM user u '
            . 'WHERE u.user_id = \'' . $id . '\';';
        $result = $connection->query($query);

        $user = null;
        if ($result->rowCount() == 1) {
            $row = $result->fetch();
            $user = new UserModel($row['user_id'], $row['first_name'],
                $row['last_name'], $row['email'], $row['password']);
        }
        $this->databaseManager->closeConnection();

        return $user;
    }

    public function insert($email, $firstName, $lastName, $password) {

        $connection = $this->databaseManager->getConnection();

        $statement = 'INSERT INTO user ('
            . 'email, first_name, last_name, password) VALUES (\''
            . $email . '\', \''
            . $firstName . '\', \''
            . $lastName . '\', \''
            . $password . '\');';
        $connection->exec($statement);

        $this->databaseManager->closeConnection();
    }
}