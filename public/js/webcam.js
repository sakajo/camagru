/**
 * Created by johann on 17.03.2017.
 */
var webcam = document.getElementById("webcam");

if (webcam !== null) {
    navigator.getUserMedia = navigator.getUserMedia
        || navigator.webkitGetUserMedia || navigator.mozGetUserMedia
        || navigator.msGetUserMedia || navigator.oGetUserMedia;

    if (navigator.getUserMedia) {
        navigator.getUserMedia({video: true}, handleVideo, videoError);
    }
}

function handleVideo(stream) {
    webcam.src = window.URL.createObjectURL(stream);
}

function videoError(e) {
    // TODO
}
